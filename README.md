## Generar la imagen de Docker

En la terminal ingresar el comando de Maven 

```
mvn compile jib:dockerBuild
```

Cuando finaliza crea la imagen con el nombre **monitoreo**

## Ejecutar la aplicación y Prometheus

```
cd docker-compose
docker-compose up
```

Esta acción inician las imagenes de la aplicación de ejemplo, Prometheus y Grafana.

En la aplicación se expone la URL con la información de Prometheus
* http://localhost:8080/actuator/prometheus


Para acceder a Prometheus ingresar en la URL: 
* http://localhost:9090
