package com.ma.ejemplo.app;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
@Log4j2
public class ObtenerNumeroRandom {

    private final Random random;
    private final int max;

    public ObtenerNumeroRandom(@Value("${app.max.random}") String maxValue) {
        random = new  Random();
        max = Integer.parseInt(maxValue);
    }

    public Integer random() {
        int next = random.nextInt(max);
        log.info("Obteniendo random {}", next);
        return next;
    }
}
