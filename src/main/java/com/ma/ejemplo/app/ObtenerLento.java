package com.ma.ejemplo.app;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
@Log4j2
public class ObtenerLento {

    private final Random random;

    public ObtenerLento() {
        random = new Random();
    }

    public String getSaludo() {
        log.info("Obtener saludo lento");
        try {
            Thread.sleep(random.nextInt(10000));
        } catch (InterruptedException e) {
            log.error("ups", e);
        }
        return "Hola";
    }
}
