package com.ma.ejemplo.app;

import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
@Log
public class ObtenerHora {

    private final String timeFormat;
    private final MeterRegistry meterRegistry;

    public ObtenerHora(@Value("${app.time.format}") String timeFormat, MeterRegistry meterRegistry) {
        this.timeFormat = timeFormat;
        this.meterRegistry = meterRegistry;
    }

    public String getHora() {
        meterRegistry.counter("hora.consultar").increment();
        log.info("dar la hora");
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(timeFormat));
    }
}
