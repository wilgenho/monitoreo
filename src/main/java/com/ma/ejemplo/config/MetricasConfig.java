package com.ma.ejemplo.config;

import com.ma.ejemplo.app.ObtenerNumeroRandom;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.Metrics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MetricasConfig {

    @Autowired
    ObtenerNumeroRandom obtenerNumeroRandom;

    @Bean
    public Gauge randomGauge() {
        return Gauge.builder("numero.random", obtenerNumeroRandom, ObtenerNumeroRandom::random)
                .register(Metrics.globalRegistry);
    }
}
