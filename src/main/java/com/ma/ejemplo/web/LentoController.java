package com.ma.ejemplo.web;

import com.ma.ejemplo.app.ObtenerLento;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Timer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicReference;

@RestController
public class LentoController {

    private final ObtenerLento lento;
    private final Timer timer;


    public LentoController(ObtenerLento lento) {
        this.lento = lento;
        timer = Timer
                .builder("mensaje.lento")
                .description("Duración para obtener un mensaje")
                .register(Metrics.globalRegistry);
    }

    @GetMapping("/lento")
    public String getMensaje() {
        AtomicReference<String> mensaje = new AtomicReference<>();
        timer.record(() -> mensaje.set(lento.getSaludo()));
        return mensaje.get();
    }
}
