package com.ma.ejemplo.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ErrorController {

    @GetMapping("/ups")
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String obtenerError() {
        return "Ups";
    }
}
