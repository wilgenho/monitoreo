package com.ma.ejemplo.web;

import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResetCounter {

    private final MeterRegistry meterRegistry;

    public ResetCounter(final MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    @GetMapping("/reset")
    public String reset() {
        meterRegistry.find("hora.consultar").meters().forEach(meterRegistry::remove);
        return "Done!";
    }
}
