package com.ma.ejemplo.web;

import com.ma.ejemplo.app.ObtenerHora;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HolaController {

    private final ObtenerHora obtenerHora;

    public HolaController(final ObtenerHora obtenerHora) {
        this.obtenerHora = obtenerHora;
    }

    @GetMapping("/hola")
    public String hola() {
        return String.format("Hola, son las %s", obtenerHora.getHora()) ;
    }
}
